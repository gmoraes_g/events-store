/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"events-store/pkg"
	"log"

	"github.com/spf13/cobra"
)

// startCmd represents the start command
var startCmd = &cobra.Command{
	Use:   "start",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {

		kafkaPool, _ := cmd.Flags().GetInt("kafkaPool")
		esPool, _ := cmd.Flags().GetInt("esPool")

		log.Println("Start events consumer")
		log.Printf("Kafka workers=%d, es workers=%d", kafkaPool, esPool)
		indexChan := make(chan *pkg.UserEventMap, 200)
		elkClient, err := pkg.ElasticSearchConn()

		if err != nil {
			log.Fatalf("Error ES connection %s", err)

		}
		wg, err := pkg.CreateWorkerPool(kafkaPool, indexChan)
		esWg := pkg.CreateIndexWorkerPool(elkClient, indexChan, esPool)

		if err != nil {
			log.Fatalf("Failed to create worker pool: %v", err)
		}

		// close(indexChan)
		wg.Wait()
		esWg.Wait()
		log.Println("All workers finished")

	},
}

func init() {
	rootCmd.AddCommand(startCmd)
	startCmd.PersistentFlags().Int("kafkaPool", 1, "Concurrency size for kakfa consumer ")
	startCmd.PersistentFlags().Int("esPool", 1, "Worker pool size for elasticsearch process")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// startCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// startCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
