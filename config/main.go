package config

import (
	"errors"
	"fmt"
	"log"
	"net"
	"net/http"
	"time"

	"github.com/elastic/go-elasticsearch/v8"
	"github.com/kelseyhightower/envconfig"
)

type KafkaConfig struct {
	Kafka struct {
		ServerList    string `envconfig:"KAFKA_SERVER"`
		Topic         string `envconfig:"KAFKA_TOPIC"`
		ConsumerGroup string `envconfig:"KAFKA_CONSUMER_GROUP"`
		// Username      string `envconfig:"KAFKA_USER"`
		// Password      string `envconfig:"KAFKA_PASS"`
	}
}

type ElasticSearchConfig struct {
	Url      string `envconfig:"ELASTICSEARCH_URL"`
	Username string `envconfig:"ELASTICSEARCH_USERNAME"`
	Password string `envconfig:"ELASTICSEARCH_PASSWORD"`
}

func LoadKafkaConfig() (*KafkaConfig, error) {
	kafkacfg := KafkaConfig{}
	err := envconfig.Process("", &kafkacfg)

	if err != nil {
		fmt.Println("Error loading Kafka config")
		return nil, errors.New("Error loading Kafka config")
	}
	log.Printf("Kafka config loaded")
	return &kafkacfg, nil
}

func LoadElasticSearchConfig() (*elasticsearch.Config, error) {
	elkcfg := ElasticSearchConfig{}
	err := envconfig.Process("", &elkcfg)
	if err != nil {
		return nil, errors.New("Error loading ELK config")
	}

	cfg := elasticsearch.Config{
		Addresses: []string{
			elkcfg.Url,
		},
		Username:      elkcfg.Username,
		Password:      elkcfg.Password,
		RetryOnStatus: []int{502, 503, 504, 429},
		MaxRetries:    5,
		Transport: &http.Transport{
			ResponseHeaderTimeout: 5 * time.Second,
			DialContext: (&net.Dialer{
				Timeout:   30 * time.Second,
				KeepAlive: 30 * time.Second,
			}).DialContext,
			MaxIdleConnsPerHost:   200,
			IdleConnTimeout:       90 * time.Second,
			ExpectContinueTimeout: 1 * time.Second,
		},
	}

	return &cfg, nil
}
