/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package main

import (
	"context"
	"events-store/cmd"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	// logFile, err := os.OpenFile("app.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	// if err != nil {
	// 	log.Fatalf("message=Error to create log file, err=%s", err)
	// }
	// log.SetOutput(logFile)
	// log.SetFlags(log.Ldate | log.Ltime | log.Lshortfile)

	cmd.Execute()
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	ctx.Done()
}
