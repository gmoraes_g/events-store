FROM golang:1.22.3

WORKDIR /app

COPY go.mod go.sum /app/

RUN go mod tidy

COPY . .

RUN GOOS=linux GOARCH=amd64 GOFLAGS="-buildvcs=false" go build events-store


CMD [ "./events-store" ,"start", "--kafkaPool=2", "--esPool=10" ]
