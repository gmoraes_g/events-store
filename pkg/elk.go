package pkg

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"events-store/config"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/elastic/go-elasticsearch/v8"
	"github.com/elastic/go-elasticsearch/v8/esapi"
)

type ElasticsearchResponse struct {
	Took     int    `json:"took"`
	TimedOut bool   `json:"timed_out"`
	Shards   Shards `json:"_shards"`
	Hits     Hits   `json:"hits"`
}

type Shards struct {
	Total      int `json:"total"`
	Successful int `json:"successful"`
	Skipped    int `json:"skipped"`
	Failed     int `json:"failed"`
}
type Hit struct {
	Index  string       `json:"_index"`
	ID     string       `json:"_id"`
	Score  float64      `json:"_score"`
	Source UserEventMap `json:"_source"`
}

type Hits struct {
	Total    TotalHits `json:"total"`
	MaxScore float64   `json:"max_score"`
	Hits     []Hit     `json:"hits"`
}

type TotalHits struct {
	Value    int    `json:"value"`
	Relation string `json:"relation"`
}

var (
	esIndex = os.Getenv("ESINDEX")
)

func ElasticSearchConn() (*elasticsearch.Client, error) {
	cfg, err := config.LoadElasticSearchConfig()

	if err != nil {
		log.Panicf("Error loading ELK config %s", err)
		return nil, errors.New("Error loading ELK config")
	}

	es, elkErr := elasticsearch.NewClient(*cfg)

	if elkErr != nil {
		log.Panicf("Error loading ELK client %s", elkErr)
		return nil, errors.New("Error loading ELK config")
	}

	return es, nil
}

func SearchDoc(tracerId string, elkClient *elasticsearch.Client) (*ElasticsearchResponse, error) {
	query := fmt.Sprintf(`{ "query": { "match": {"tracer_id": "%s" } } }`, tracerId)
	esr := ElasticsearchResponse{}

	res, err := elkClient.Search(
		elkClient.Search.WithIndex(esIndex),
		elkClient.Search.WithBody(strings.NewReader(query)),
	)
	defer res.Body.Close()
	if err != nil {
		log.Printf("message=Error searching document, tracer_id=%s, err=%s", tracerId, err)
		return nil, errors.New("message=Error searching document")
	}
	if res.StatusCode == http.StatusOK {

		bodyBytes, err := io.ReadAll(res.Body)

		if err != nil {
			log.Println(err)
		}
		jsonError := json.Unmarshal(bodyBytes, &esr)
		log.Printf("tracer_id found, tracer_id=%s, total_values=%d", tracerId, esr.Hits.Total.Value)

		if jsonError != nil {
			log.Printf("Error to map message %s", jsonError)

		}

	}
	return &esr, nil

}

func UpdateDocument(esResponse *ElasticsearchResponse, user *UserEventMap, elkClient *elasticsearch.Client) bool {
	esHit := esResponse.Hits
	if esHit.Total.Value == 0 {
		log.Printf("message=Retry search doc, tracer_id=%s", user.TracerId)
		return false

	} else {
		for _, hit := range esResponse.Hits.Hits {
			log.Printf("message=Looking for the tracer id, hit_tracer_id=%s, user_tracer_id=%s", hit.Source.TracerId, user.TracerId)
			if hit.Source.TracerId == user.TracerId {
				doc_id := hit.ID
				log.Printf("message=Starting update index, tracer_id=%s, doc_id=%s, body=%s", user.TracerId, doc_id, hit.Source)
				updateField := map[string]interface{}{
					"doc": map[string]interface{}{
						"SuccededAt": user.Timestamp.Format(time.RFC3339),
						"event":      user.Event,
					},
				}
				userUpdateBody, _ := json.Marshal(updateField)
				req := esapi.UpdateRequest{
					Index:      esIndex,
					DocumentID: doc_id,
					Body:       bytes.NewReader(userUpdateBody),
					Refresh:    "wait_for",
				}
				res, err := req.Do(context.Background(), elkClient)
				bodyBytes, _ := io.ReadAll(res.Body)
				bodyString := string(bodyBytes)

				if err != nil || res.StatusCode >= 400 {
					log.Printf("message=Error updating index, tracer_id=%s, doc_id=%s, err=%s, body=%s", user.TracerId, doc_id, err, bodyString)
				} else {
					log.Printf("message=Successfully update, tracer_id=%s, doc_id=%s, body=%s", user.TracerId, doc_id, bodyString)
					return true
				}
			}

		}
		log.Printf("tracer_id not match == %s", user.TracerId)
		return false
	}

}

func IndexDocument(user *UserEventMap, elkClient *elasticsearch.Client) error {

	data, _ := json.Marshal(user)

	res, indexErr := elkClient.Index(
		esIndex,
		bytes.NewReader(data),
		// elkClient.Index.WithRefresh("wait_for"),
	)

	if indexErr != nil {
		log.Printf("Error indexing %s", indexErr)
		return indexErr

	}
	indexStatus := res.StatusCode
	if indexStatus > 201 {
		log.Printf("message=Error indexing document, status_code=%d", indexStatus)
	}
	bodyBytes, _ := io.ReadAll(res.Body)
	bodyString := string(bodyBytes)
	res.Body.Close()
	log.Printf("Indexing successed, body=%s", bodyString)
	return nil
}
