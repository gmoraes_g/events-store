package pkg

import (
	"events-store/config"
	"log"
	"time"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/elastic/go-elasticsearch/v8"
)

type UserEventMap struct {
	Name        string    `json:"name"`
	LastName    string    `json:"last_name"`
	Email       string    `json:"email"`
	TracerId    string    `json:"tracer_id"`
	App         string    `json:"app"`
	Timestamp   time.Time `json:"timestamp" format:"2006-01-02T15:04:05Z07:00"`
	Event       string    `json:"event"`
	Key         string
	Deliveredat time.Time `format:"2006-01-02T15:04:05Z07:00"`
	SuccededAt  time.Time `format:"2006-01-02T15:04:05Z07:00"`
}

type UserEventMapRetry struct {
	Count int
	User  UserEventMap
}

func KafkaConnect() (*kafka.Consumer, error) {
	kafkacfg, err := config.LoadKafkaConfig()

	log.Printf("Start connection on server %s", kafkacfg.Kafka.ServerList)
	if err != nil {
		log.Panicf("Error loading kafka config: %s", err)
		return nil, err
	}

	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": kafkacfg.Kafka.ServerList,
		"group.id":          kafkacfg.Kafka.ConsumerGroup,
		"auto.offset.reset": "earliest",
		// "security.protocol": "PLAINTEXT",
		// "sasl.mechanisms":   "PLAIN",
		// "sasl.username":     kafkacfg.Kafka.Username,
		// "sasl.password":     kafkacfg.Kafka.Password,
	})

	if err != nil {
		log.Panicf("Connection error: %s", err)
		return nil, err
	}

	topic := kafkacfg.Kafka.Topic
	err = c.SubscribeTopics([]string{topic}, nil)
	if err != nil {
		log.Printf("Error subscribing topic %s, erro=%s", topic, err)
		return nil, err
	}

	return c, nil

}

func saveEvent(user *UserEventMap, elkClient *elasticsearch.Client) error {
	log.Printf("Start message persistency, %s from app=%s", *user, user.Key)
	if user.Event == "DELIVERED" {
		log.Printf("Fist, tracer_id=%s", user.TracerId)
		user.Deliveredat = user.Timestamp
		err := IndexDocument(user, elkClient)
		if err != nil {
			log.Printf("Error indexing customer, %s, error=%s", user, err)
			return err
		}
	}

	if user.Event == "SUCCEED" {

		log.Printf("searching by tracer id, tracer_id=%s", user.TracerId)

		for i := 0; i <= 10; i++ {
			esResponse, err := SearchDoc(user.TracerId, elkClient)
			if err != nil {
				log.Println(err)
			}
			isUpdated := UpdateDocument(esResponse, user, elkClient)

			if !isUpdated {
				log.Printf("message=Retrying tracer_id=%s, qtd_retry=%d", user.TracerId, i)
				time.Sleep(10 * time.Millisecond)

			} else {
				return nil
			}

		}
		log.Printf("Retry not work, tracer_id=%s", user.TracerId)

	}

	return nil
}
