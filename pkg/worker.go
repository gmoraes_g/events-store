package pkg

import (
	"encoding/json"
	"fmt"
	"log"
	"sync"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/elastic/go-elasticsearch/v8"
)

// type Task interface {
// 	Process()
// }

type Worker struct {
	consumer *kafka.Consumer
	wg       *sync.WaitGroup
	esChan   chan *UserEventMap
}

func NewWorker(wg *sync.WaitGroup, esChan chan *UserEventMap) (*Worker, error) {
	// Kafka new consumer
	consumer, err := KafkaConnect()
	if err != nil {
		log.Printf("Error kafka connection %s", err)
		return nil, err
	}
	// Elasticsearch new connection

	worker := Worker{
		consumer: consumer,
		wg:       wg,
		esChan:   esChan,
	}

	return &worker, nil
}

func (w *Worker) Run() {
	defer w.wg.Done()
	defer w.consumer.Close()
	for {

		log.Println("Waiting message")
		msg, err := w.consumer.ReadMessage(-1)
		if err != nil {
			fmt.Println("Erro during message consuming")
		}
		fmt.Println(msg.TopicPartition)
		key := string(msg.Key)
		userEventMap := UserEventMap{Key: key}
		jsonError := json.Unmarshal(msg.Value, &userEventMap)
		if jsonError != nil {
			log.Printf("Error to map message %s", jsonError)

		} else {
			log.Println("Message consumed and sent to ES channel")
			w.esChan <- &userEventMap

			if err != nil {
				log.Printf("message=Error saving events, err=%s", err)
			}

		}

	}

}
func CreateIndexWorkerPool(elkClient *elasticsearch.Client, indexChan chan *UserEventMap, numWorkers int) *sync.WaitGroup {
	var wg sync.WaitGroup
	for i := 0; i < numWorkers; i++ {
		log.Printf("ES worker created with id=%d", i)
		wg.Add(1)
		go runIndexWorker(elkClient, indexChan, &wg)
	}
	return &wg
}

func runIndexWorker(elkClient *elasticsearch.Client, indexChan chan *UserEventMap, wg *sync.WaitGroup) {
	defer wg.Done()
	for userEventMap := range indexChan {
		log.Println("Send event to elasticsearch")
		err := saveEvent(userEventMap, elkClient)

		if err != nil {
			fmt.Println(err)
		}
	}
}

func CreateWorkerPool(partitions int, indexChan chan *UserEventMap) (*sync.WaitGroup, error) {
	var wg sync.WaitGroup

	for i := 0; i < partitions; i++ {
		wg.Add(1)
		worker, err := NewWorker(&wg, indexChan)
		if err != nil {
			return nil, err
		}
		go worker.Run()
	}

	return &wg, nil
}
