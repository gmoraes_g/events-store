module events-store

go 1.22.3

require (
	github.com/confluentinc/confluent-kafka-go v1.9.2
	github.com/elastic/go-elasticsearch/v7 v7.17.10
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/spf13/cobra v1.8.0
)

require (
	github.com/elastic/elastic-transport-go/v8 v8.6.0 // indirect
	github.com/elastic/go-elasticsearch/v8 v8.14.0 // indirect
	github.com/go-logr/logr v1.4.1 // indirect
	github.com/go-logr/stdr v1.2.2 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	go.opentelemetry.io/otel v1.24.0 // indirect
	go.opentelemetry.io/otel/metric v1.24.0 // indirect
	go.opentelemetry.io/otel/trace v1.24.0 // indirect
)
